package com.bestbuy.geeksquad.remoteservices;

import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class RestTemplateFactory  {
	//API call timeout in seconds
	static final int DEFAULT_WS_TIMEOUT = 120;


	public RestTemplate newRestTemplate() {

		SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
		int timeoutMilliseconds = DEFAULT_WS_TIMEOUT * 1000;
		requestFactory.setConnectTimeout(timeoutMilliseconds);
		requestFactory.setReadTimeout(timeoutMilliseconds);

		RestTemplate restTemplate = new RestTemplate(requestFactory);
		RestErrorHandler errorHandler = new RestErrorHandler();
		restTemplate.setErrorHandler(errorHandler);

		return restTemplate;
	}

}
