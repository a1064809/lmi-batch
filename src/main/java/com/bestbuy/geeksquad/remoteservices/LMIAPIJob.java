package com.bestbuy.geeksquad.remoteservices;

import java.io.File;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/*
 * Author: Sid
 * This is a scheduled job. Job interval is configured in applicationContext.xml.
 * Whenever this Job runs it makes list of API calls in below sequence get Agent's LMI hours.
 * Parse the report output and dump it in DB
 */
public class LMIAPIJob extends Base{

	private static final Logger LOG = LoggerFactory.getLogger(LMIAPIJob.class);
	private static final String REPORT_TYPE = "2";

	public void process() {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		LOG.info("Start of LMI Hours Job:"
				+ dateFormat.format(System.currentTimeMillis()));

		try {

			preProcess();
			
			String authCode = getAuthCode(USER_ID, PASSWORD);

            String lastRunDateSql = "select max(JOB_RUN_DATE) from " + DB_SCHEMA + ".AGENT_LMI_HOUR";

            // get last run date for this report
            Date lastRunDate = getLastRunDate( JDBC_URL, JDBC_USERID, JDBC_PASSWORD,DB_SCHEMA, lastRunDateSql);

            Date endDate = DateUtils.addDays(new Date(), -1);

            SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");

            Date begDate = DateUtils.addDays(new Date(), -7);

            // check if begin Date is after last run date, if so change beginDate to lastRunDate
            if(begDate.after(lastRunDate)){
                begDate = lastRunDate;
            }
			setReportDate(formatter.format(begDate), formatter.format(endDate), authCode);

			setReportTime("00:00:00", "23:59:59", authCode);

			Calendar c = Calendar.getInstance();
			c.setTime(new Date()); // your date; omit this line for current date
			int offset = c.get(Calendar.DST_OFFSET);

			// offset 0 means no DST, any other value (most likely 3600000)
			// means that this date is affected by DST
			// Haven't tested in DST time - hope it works
			setReportTimeZone((offset == 0 ? "360" : "300"), authCode);

			setReportArea(REPORT_TYPE, authCode);

			setReportOutputType(OUTPUT_TYPE_XML, authCode);

			String reportText = getReport(CHANNEL_ID, authCode);

			String currentDateTime = (new SimpleDateFormat("ddMMyyyy_HH-mm-ss"))
					.format(System.currentTimeMillis());
			FileUtils.writeStringToFile(new File(OUTPUT_FOLDER + "LMIHOURS"
					+ currentDateTime + "." + OUTPUT_TYPE_XML), cleanXMLString(reportText));
			LOG.info("Report is written to file:" + OUTPUT_FOLDER + "LMIHOURS"
					+ currentDateTime + "." + OUTPUT_TYPE_XML);

			List<IData> dataList = parseReportData(reportText);

			if (dataList == null || dataList.size() == 0)
				LOG.info("No rows found in the XML");
			else
				insertIntoDB(dataList, JDBC_URL, JDBC_USERID, JDBC_PASSWORD, DB_SCHEMA);
		} catch (Exception e) {
			LOG.error("Unexpected error occured in the job: " + e);
		}

		LOG.info("End of Job:" + dateFormat.format(System.currentTimeMillis()));
	}

	

	

	protected List<IData> parseReportData(String reportXMLStr) throws Exception {

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		List<IData> dataList = null;
		DocumentBuilder builder;
		builder = factory.newDocumentBuilder();
		Document document = builder.parse(new InputSource(new StringReader(
				reportXMLStr)));
		NodeList dataNode = document.getElementsByTagName("data");
		NodeList rowNodeList = ((Element) dataNode.item(0))
				.getElementsByTagName("row");
		dataList = new ArrayList<IData>();
		for (int i = 0; i < rowNodeList.getLength(); i++) {
			LMIData data = new LMIData();
			Element rowNode = (Element) rowNodeList.item(i);
			NodeList fieldNodeList = rowNode.getElementsByTagName("field");
			for (int j = 0; j < fieldNodeList.getLength(); j++) {
				Element fieldNode = (Element) fieldNodeList.item(j);
				try {
					int attValue = Integer.parseInt(fieldNode
							.getAttribute("id"));
					switch (attValue) {
					case 0:
						data.setLoginDate(fieldNode.getTextContent());
						break;
					case 1:
						data.setName(fieldNode.getTextContent());
						break;
					case 2:
						data.setUserId(fieldNode.getTextContent());
						break;
					case 4:
						data.setStartTime(fieldNode.getTextContent());
						break;
					case 5:
						data.setEndTime(fieldNode.getTextContent());
						break;
					case 6:
						data.setTotalLoginTime(fieldNode.getTextContent());
						break;
					case 7:
						data.setIpAddress(fieldNode.getTextContent());
						break;
					case 8:
						data.setBusyTime(fieldNode.getTextContent());
						break;
					case 9:
						data.setAwayTime(fieldNode.getTextContent());
						break;
					case 10:
						data.setIdleTime(fieldNode.getTextContent());
						break;
					}
				} catch (NumberFormatException e) {

				}
			}
			dataList.add(data);
		}
		LOG.debug("dataList:" + dataList);
		return dataList;
	}

	protected void insertIntoDB(List<IData> dataList, String jdbcURL,
			String jdbcUserId, String jdbcPassword, String dbSchema) throws Exception {
		Connection dbCon = null;
		Statement stmt = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;

		String selectQuery = "select count(*) from " + dbSchema + ".AGENT_LMI_HOUR where JOB_RUN_DATE = trunc(sysdate)";
		String insertQuery = "INSERT INTO " + dbSchema + ".AGENT_LMI_HOUR(JOB_RUN_DATE, LOGIN_DATE, NAME ,USER_ID, EMAIL ,START_TIME, END_TIME, TOTAL_LOGIN_TIME, IP_ADDRESS, BUSY_TIME, AWAY_TIME, IDLE_TIME, CREATE_DATE) VALUES(trunc(sysdate), to_date(?, 'MM/DD/YYYY'),?,?,?,to_date(?, 'MM/DD/YYYY HH12:MI:SS AM'),to_date(?, 'MM/DD/YYYY HH12:MI:SS AM'),?,?,?,?,?,sysdate)";
		try {
			dbCon = getDBConnection(jdbcURL, jdbcUserId, jdbcPassword);
			dbCon.setAutoCommit(false);
			stmt = dbCon.createStatement();
			rs = stmt.executeQuery(selectQuery);
			int noOfRows = 0;
			if (rs != null && rs.next()) {
				noOfRows = rs.getInt(1);
			}

			if (noOfRows > 0) {
				LOG.info("Stopped further processing. Job has already run for yesterday's LOGIN_DATE.");
			} else {
				LOG.info("Inserting records into database:" + dataList.size());
				int recCount = 0;
				pstmt = dbCon.prepareStatement(insertQuery);
				for (IData obj : dataList) {
					LMIData data = (LMIData)obj;
					if(++recCount % 100 == 0){
						pstmt.executeBatch();
						pstmt.clearBatch();
					}
					pstmt.setString(1, data.getLoginDate());
					
					if(data.getName() != null)
						pstmt.setString(2, data.getName());
					else
						pstmt.setNull(2, Types.VARCHAR);
					
					if(data.getUserId() != null)
						pstmt.setString(3, data.getUserId());
					else
						pstmt.setNull(3, Types.VARCHAR);
					
					if(data.getEmail() != null)
						pstmt.setString(4, data.getEmail());
					else
						pstmt.setNull(4, Types.VARCHAR);
					
					if(data.getStartTime() != null)
						pstmt.setString(5, data.getStartTime());
					else
						pstmt.setNull(5, Types.TIMESTAMP);	
					
					if(data.getEndTime() != null)
						pstmt.setString(6, data.getEndTime());
					else
						pstmt.setNull(6, Types.TIMESTAMP);
					
					if(data.getTotalLoginTime() != null)
						pstmt.setString(7, data.getTotalLoginTime());
					else
						pstmt.setNull(7, Types.VARCHAR);
					
					if(data.getIpAddress() != null)
						pstmt.setString(8, data.getIpAddress());
					else
						pstmt.setNull(8, Types.VARCHAR);
					
					if(data.getBusyTime() != null)
						pstmt.setString(9, data.getBusyTime());
					else
						pstmt.setNull(9, Types.VARCHAR);

					if(data.getAwayTime() != null)
						pstmt.setString(10, data.getAwayTime());
					else
						pstmt.setNull(10, Types.VARCHAR);

					if(data.getIdleTime() != null)
						pstmt.setString(11, data.getIdleTime());
					else
						pstmt.setNull(11, Types.VARCHAR);

					pstmt.addBatch();
				}
				if(recCount != 0)
					pstmt.executeBatch();
				dbCon.commit();
			}
		}catch(Exception e){ 
			if (dbCon != null)
				dbCon.rollback();
			throw e;
		}finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception ignore) {
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (Exception ignore) {
				}
			}
			if (dbCon != null) {
				try {
					dbCon.close();
				} catch (Exception ignore) {
				}
			}
		}

	}

	private static Connection getDBConnection(String jdbcURL,
			String jdbcUserId, String jdbcPassword) throws Exception {

		Connection dbConnection = null;

		try {

			Class.forName("oracle.jdbc.driver.OracleDriver");

			dbConnection = DriverManager.getConnection(jdbcURL, jdbcUserId,
					jdbcPassword);
			return dbConnection;

		} catch (Exception e) {

			LOG.error("Error in creating DB connection");
			throw e;

		}

	}

	private class LMIData implements IData{
		public String getLoginDate() {
			return loginDate;
		}

		public void setLoginDate(String loginDate) {
			this.loginDate = loginDate;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getUserId() {
			return userId;
		}

		public void setUserId(String userId) {
			this.userId = userId;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getStartTime() {
			return startTime;
		}

		public void setStartTime(String startTime) {
			this.startTime = startTime;
		}

		public String getEndTime() {
			return endTime;
		}

		public void setEndTime(String endTime) {
			this.endTime = endTime;
		}

		public String getTotalLoginTime() {
			return totalLoginTime;
		}

		public void setTotalLoginTime(String totalLoginTime) {
			this.totalLoginTime = totalLoginTime;
		}

		public String getIpAddress() {
			return ipAddress;
		}

		public void setIpAddress(String ipAddress) {
			this.ipAddress = ipAddress;
		}

		public String getBusyTime() {
			return busyTime;
		}

		public void setBusyTime(String busyTime) {
			this.busyTime = busyTime;
		}

		public String getAwayTime() {
			return awayTime;
		}

		public void setAwayTime(String awayTime) {
			this.awayTime = awayTime;
		}

		public String getIdleTime() {
			return idleTime;
		}

		public void setIdleTime(String idleTime) {
			this.idleTime = idleTime;
		}

		private String loginDate;
		private String name;
		private String userId;
		private String email;
		private String startTime;
		private String endTime;
		private String totalLoginTime;
		private String ipAddress;
		private String busyTime;
		private String awayTime;
		private String idleTime;
	}
	
	public static void main(String[] args) {
		try {
			String reportString = FileUtils.readFileToString(new File("C:\\LMIReport12122014_15-15-27.XML"));
			System.out.println(reportString);
			LMIAPIJob job = new LMIAPIJob();
			List<IData> dataList = job.parseReportData(reportString);
			job.insertIntoDB(dataList, "jdbc:oracle:thin:@localhost:1521:vasu", "stargate_jbpm", "bobby_jbpm", "ANDYG_DEV");
			System.out.println(dataList);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}