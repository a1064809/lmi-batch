package com.bestbuy.geeksquad.remoteservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;

@Controller
public class LMIJobRESTAPI {

	@Autowired
	@Qualifier("LMIHoursJob")
	private LMIAPIJob lmiHoursJob;
	
	@Autowired
	@Qualifier("LMIAPIFeedbackJob")
	private LMIAPIFeedbackJob lmiFeebackJob;

    @Autowired
    @Qualifier("LMIAgentHierarchyJob")
    private LMIAPIAgentHierarchyJob lmiAgentHierarchyJob;
	
	private static final Logger LOG = LoggerFactory.getLogger(LMIJobRESTAPI.class);
	
	@RequestMapping(value = "runLMIHours", method = RequestMethod.GET)
    public @ResponseBody String getLMIHours() {
        LOG.info("Starting job through REST call");
        lmiHoursJob.process();
        LOG.info("Job ended through REST call"); 
        return "Job Execution Complete";
    }
	
        @RequestMapping(value = "runLMISurvey", method = RequestMethod.GET)
    public @ResponseBody String getLMISurvey() {
        LOG.info("Starting job through REST call");
        lmiFeebackJob.process();
        LOG.info("Job ended through REST call"); 
        return "Job Execution Complete";
    }

    @RequestMapping(value = "runLMIAgentHierarchy", method = RequestMethod.GET)
    public @ResponseBody String getLMIAgentHierarchy() {
        LOG.info("Starting getLMIAgentHierarchy job through REST call");
        lmiAgentHierarchyJob.process();
        LOG.info("Job getLMIAgentHierarchy ended through REST call");
        return "Job getLMIAgentHierarchy Execution Complete";
    }
}
