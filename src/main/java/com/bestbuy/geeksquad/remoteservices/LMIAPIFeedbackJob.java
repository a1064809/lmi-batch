package com.bestbuy.geeksquad.remoteservices;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Handler;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/*
 * Author: Sid
 * This is a scheduled job. Job interval is configured in applicationContext.xml.
 * Whenever this Job runs it makes list of API calls in below sequence get Agent's LMI hours.
 * Parse the report output and dump it in DB
 */
public class LMIAPIFeedbackJob extends Base {

	private static final Logger LOG = LoggerFactory
			.getLogger(LMIAPIFeedbackJob.class);
	private static final String REPORT_TYPE = "1";

	public void process() {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		LOG.info("Start of LMI Feedback Job:"
				+ dateFormat.format(System.currentTimeMillis()));

		try {

			preProcess();
			String authCode = getAuthCode(USER_ID, PASSWORD);

            String lastRunDateSql = "select max(JOB_RUN_DATE) from " + DB_SCHEMA + ".AGENT_LMI_FEEDBACK";

            // get last run date for this report
            Date lastRunDate = getLastRunDate( JDBC_URL, JDBC_USERID, JDBC_PASSWORD,DB_SCHEMA, lastRunDateSql);

			Date endDate = DateUtils.addDays(new Date(), -1);

			SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");

			Date begDate = DateUtils.addDays(new Date(), -7);

            // check if begin Date is after last run date, if so change beginDate to lastRunDate
            if(begDate.after(lastRunDate)){
                begDate = lastRunDate;
            }
			setReportDate(formatter.format(begDate), formatter.format(endDate),
					authCode);

			setReportTime("00:00:00", "23:59:59", authCode);

			Calendar c = Calendar.getInstance();
			c.setTime(new Date()); // your date; omit this line for current date
			int offset = c.get(Calendar.DST_OFFSET);

			// offset 0 means no DST, any other value (most likely 3600000)
			// means that this date is affected by DST
			// Haven't tested in DST time - hope it works
			setReportTimeZone((offset == 0 ? "360" : "300"), authCode);

			setReportArea(REPORT_TYPE, authCode);

			setReportOutputType(OUTPUT_TYPE_XML, authCode);

			String reportText = getReport(CHANNEL_ID, authCode);

			String currentDateTime = (new SimpleDateFormat("ddMMyyyy_HH-mm-ss"))
					.format(System.currentTimeMillis());
			FileUtils.writeStringToFile(new File(OUTPUT_FOLDER + "LMIFEEDBACK"
					+ currentDateTime + "." + OUTPUT_TYPE_XML), cleanXMLString(reportText));
			LOG.info("Report is written to file:" + OUTPUT_FOLDER  + "LMIFEEDBACK"
					+ currentDateTime + "." + OUTPUT_TYPE_XML);

			List<IData> dataList = parseReportData(reportText);

			if (dataList == null || dataList.size() == 0)
				LOG.info("No rows found in the XML");
			else
				insertIntoDB(dataList, JDBC_URL, JDBC_USERID, JDBC_PASSWORD,
						DB_SCHEMA);

		} catch (Exception e) {
			LOG.error("Unexpected error occured in the job: " + e);
		}

		LOG.info("End of LMI Feedback Job:" + dateFormat.format(System.currentTimeMillis()));
	}

	protected List<IData> parseReportData(String reportXMLStr) throws Exception {

		StringBuffer tempStr = new StringBuffer().append("<root>").append(reportXMLStr).append("</root>");
		reportXMLStr = tempStr.toString();
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		List<IData> dataList = null;
		DocumentBuilder builder;
		builder = factory.newDocumentBuilder();

		InputStream inputStream = new ByteArrayInputStream(
				reportXMLStr.getBytes());
		Reader reader = new InputStreamReader(inputStream, "UTF-8");

		InputSource is = new InputSource(reader);
		is.setEncoding("UTF-8");

		Document document = builder.parse(is);
		dataList = new ArrayList<IData>();
		NodeList reportNodeList = document.getElementsByTagName("report");
		for (int z = 0; z < reportNodeList.getLength(); z++) {

			NodeList headerNode = ((Element) reportNodeList.item(z))
					.getElementsByTagName("header");
			NodeList columns = ((Element) headerNode.item(0))
					.getElementsByTagName("field");

			if (columns.getLength() >= 14) {

				NodeList dataNode = ((Element) reportNodeList.item(z))
						.getElementsByTagName("data");
				NodeList rowNodeList = ((Element) dataNode.item(0))
						.getElementsByTagName("row");

				for (int i = 0; i < rowNodeList.getLength(); i++) {
					LMIFeedbackData data = new LMIFeedbackData();
					Element rowNode = (Element) rowNodeList.item(i);
					NodeList fieldNodeList = rowNode
							.getElementsByTagName("field");
					for (int j = 0; j < fieldNodeList.getLength(); j++) {
						Element fieldNode = (Element) fieldNodeList.item(j);
						try {
							int attValue = Integer.parseInt(fieldNode
									.getAttribute("id"));
							switch (attValue) {
							case 0:
								data.setSource(fieldNode.getTextContent());
								break;
							case 1:
								data.setSessionId(fieldNode.getTextContent());
								break;
							case 2:
								data.setDate(fieldNode.getTextContent());
								break;
							case 3:
								data.setName(fieldNode.getTextContent());
								break;
							case 4:
								data.setQ1(fieldNode.getTextContent());
								break;
							case 5:
								data.setQ2(fieldNode.getTextContent());
								break;
							case 6:
								data.setQ3(fieldNode.getTextContent());
								break;
							case 7:
								data.setQ4(fieldNode.getTextContent());
								break;
							case 8:
								data.setQ5(fieldNode.getTextContent());
								break;
							case 9:
								data.setQ6(fieldNode.getTextContent());
								break;
							case 10:
								data.setQ7(fieldNode.getTextContent());
								break;
							case 11:
								data.setQ8(fieldNode.getTextContent());
								break;
							case 12:
								data.setFeedBackFolluUpFlag(fieldNode
										.getTextContent());
								break;
							case 44:
								data.setTechnicianName(fieldNode
										.getTextContent());
								break;
							case 45:
								data.setTechnicianId(fieldNode.getTextContent());
								break;
							}
						} catch (NumberFormatException e) {

						}
					}
					dataList.add(data);
				}
			}
		}
		//LOG.debug("dataList:" + dataList);
		return dataList;
	}

	protected void insertIntoDB(List<IData> dataList, String jdbcURL,
			String jdbcUserId, String jdbcPassword, String dbSchema)
			throws Exception {
		Connection dbCon = null;
		Statement stmt = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;

		String selectQuery = "select count(*) from " + dbSchema
				+ ".AGENT_LMI_FEEDBACK where JOB_RUN_DATE = trunc(sysdate)";
		String insertQuery = "INSERT INTO "
				+ dbSchema
				+ ".AGENT_LMI_FEEDBACK(JOB_RUN_DATE, SOURCE, SESSION_ID, FEEDBACK_DATE, NAME, Q1, Q2, Q3, Q4, Q5, Q6, Q7, Q8, FEEDBACK_FOLLOWUP_FLAG, TECHNICIAN_NAME, TECHNICIAN_ID,  CREATE_DATE) VALUES(trunc(sysdate), ?, ?, to_date(?, 'MM/DD/YYYY HH:MI:SS AM'),?,?,?,?,?,?,?,?,?,?,?,?,sysdate)";
		try {
			dbCon = getDBConnection(jdbcURL, jdbcUserId, jdbcPassword);
			dbCon.setAutoCommit(false);
			stmt = dbCon.createStatement();
			rs = stmt.executeQuery(selectQuery);
			int noOfRows = 0;
			if (rs != null && rs.next()) {
				noOfRows = rs.getInt(1);
			}

			if (noOfRows > 0) {
				LOG.info("Stopped further processing. Job has already run for yesterday's LOGIN_DATE.");
			} else {
				LOG.info("Inserting records into database:" + dataList.size());
				int recCount = 0;
				pstmt = dbCon.prepareStatement(insertQuery);
				for (IData obj : dataList) {
					LMIFeedbackData data = (LMIFeedbackData) obj;
					if (++recCount % 100 == 0) {
						pstmt.executeBatch();
						pstmt.clearBatch();
					}

					if (data.getSource() != null)
						pstmt.setString(1, data.getSource());
					else
						pstmt.setNull(1, Types.VARCHAR);
					pstmt.setString(1, data.getSource());

					if (data.getSessionId() != null)
						pstmt.setString(2, data.getSessionId());
					else
						pstmt.setNull(2, Types.VARCHAR);

					if (data.getDate() != null)
						pstmt.setString(3, data.getDate());
					else
						pstmt.setNull(3, Types.TIMESTAMP);

					if (data.getName() != null)
						pstmt.setString(4, data.getName());
					else
						pstmt.setNull(4, Types.VARCHAR);

					if (data.getQ1() != null)
						pstmt.setString(5, data.getQ1());
					else
						pstmt.setNull(5, Types.INTEGER);

					if (data.getQ2() != null)
						pstmt.setString(6, data.getQ2());
					else
						pstmt.setNull(6, Types.INTEGER);
					if (data.getQ3() != null)
						pstmt.setString(7, data.getQ3());
					else
						pstmt.setNull(7, Types.INTEGER);
					if (data.getQ4() != null)
						pstmt.setString(8, data.getQ4());
					else
						pstmt.setNull(8, Types.INTEGER);
					if (data.getQ5() != null)
						pstmt.setString(9, data.getQ5());
					else
						pstmt.setNull(9, Types.INTEGER);
					if (data.getQ6() != null)
						pstmt.setString(10, data.getQ6());
					else
						pstmt.setNull(10, Types.INTEGER);
					if (data.getQ7() != null)
						pstmt.setString(11, data.getQ7());
					else
						pstmt.setNull(11, Types.INTEGER);
					if (data.getQ8() != null)
						pstmt.setString(12, data.getQ8());
					else
						pstmt.setNull(12, Types.VARCHAR);

					if (data.getFeedBackFolluUpFlag() != null)
						pstmt.setString(13, data.getFeedBackFolluUpFlag());
					else
						pstmt.setNull(13, Types.INTEGER);

					if (data.getTechnicianName() != null)
						pstmt.setString(14, data.getTechnicianName());
					else
						pstmt.setNull(14, Types.VARCHAR);

					if (data.getTechnicianId() != null)
						pstmt.setString(15, data.getTechnicianId());
					else
						pstmt.setNull(15, Types.VARCHAR);

					pstmt.addBatch();
				}
				if (recCount != 0)
					pstmt.executeBatch();
				dbCon.commit();
			}
		} catch (Exception e) {
			if (dbCon != null)
				dbCon.rollback();
			throw e;
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception ignore) {
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (Exception ignore) {
				}
			}
			if (dbCon != null) {
				try {
					dbCon.close();
				} catch (Exception ignore) {
				}
			}
		}

	}

	private static Connection getDBConnection(String jdbcURL,
			String jdbcUserId, String jdbcPassword) throws Exception {

		Connection dbConnection = null;

		try {

			Class.forName("oracle.jdbc.driver.OracleDriver");

			dbConnection = DriverManager.getConnection(jdbcURL, jdbcUserId,
					jdbcPassword);
			return dbConnection;

		} catch (Exception e) {

			LOG.error("Error in creating DB connection");
			throw e;

		}

	}

	private class LMIFeedbackData implements IData {

		private String source;

		public String getSource() {
			return source;
		}

		public void setSource(String source) {
			this.source = source;
		}

		public String getSessionId() {
			return sessionId;
		}

		public void setSessionId(String sessionId) {
			this.sessionId = sessionId;
		}

		public String getDate() {
			return date;
		}

		public void setDate(String date) {
			this.date = date;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getQ1() {
			return q1;
		}

		public void setQ1(String q1) {
			this.q1 = q1;
		}

		public String getQ2() {
			return q2;
		}

		public void setQ2(String q2) {
			this.q2 = q2;
		}

		public String getQ3() {
			return q3;
		}

		public void setQ3(String q3) {
			this.q3 = q3;
		}

		public String getQ4() {
			return q4;
		}

		public void setQ4(String q4) {
			this.q4 = q4;
		}

		public String getQ5() {
			return q5;
		}

		public void setQ5(String q5) {
			this.q5 = q5;
		}

		public String getQ6() {
			return q6;
		}

		public void setQ6(String q6) {
			this.q6 = q6;
		}

		public String getQ7() {
			return q7;
		}

		public void setQ7(String q7) {
			this.q7 = q7;
		}

		public String getQ8() {
			return q8;
		}

		public void setQ8(String q8) {
			this.q8 = q8;
		}

		public String getFeedBackFolluUpFlag() {
			return feedBackFolluUpFlag;
		}

		public void setFeedBackFolluUpFlag(String feedBackFolluUpFlag) {
			this.feedBackFolluUpFlag = feedBackFolluUpFlag;
		}

		public String getTechnicianId() {
			return technicianId;
		}

		public void setTechnicianId(String technicianId) {
			this.technicianId = technicianId;
		}

		public String getTechnicianName() {
			return technicianName;
		}

		public void setTechnicianName(String technicianName) {
			this.technicianName = technicianName;
		}

		private String sessionId;
		private String date;
		private String name;
		private String q1;
		private String q2;
		private String q3;
		private String q4;
		private String q5;
		private String q6;
		private String q7;
		private String q8;
		private String feedBackFolluUpFlag;
		private String technicianName;
		private String technicianId;
	}

	public static void main(String[] args) {
		try {
			String reportString = FileUtils.readFileToString(new File(
					"C:\\LMIReport12052015_23-01-44.XML"));
			// System.out.println(reportString);
			LMIAPIFeedbackJob job = new LMIAPIFeedbackJob();
			List<IData> dataList = job.parseReportData(reportString);
			job.insertIntoDB(dataList, "jdbc:oracle:thin:@localhost:1521:vasu",
					"stargate_jbpm", "bobby_jbpm", "ANDYG_DEV");
			System.out.println(dataList);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}