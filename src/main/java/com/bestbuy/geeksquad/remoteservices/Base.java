package com.bestbuy.geeksquad.remoteservices;

import java.io.File;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/*
 * Author: Sid
 * This is a base class LMI API related jobs.
 */
public abstract class Base {

	private static final Logger LOG = LoggerFactory.getLogger(Base.class);
	@Autowired
	RestTemplateFactory restTemplateFactory;

	@Value("${lmi.baseurl}")
	public String BASE_URL;

	@Value("${lmi.userid}")
	public String USER_ID;

	@Value("${lmi.password}")
	public String PASSWORD;

	@Value("${lmi.outputype.xml}")
	public String OUTPUT_TYPE_XML;

    @Value("${lmi.outputype.txt}")
    public String OUTPUT_TYPE_TXT;

	@Value("${lmi.channelid}")
	public String CHANNEL_ID;

	@Value("${lmi.reportoutputfolder}")
	public String OUTPUT_FOLDER;

	@Value("${lmi.jdbcURL}")
	public String JDBC_URL;

	@Value("${lmi.jdbcUserId}")
	public String JDBC_USERID;

	@Value("${lmi.jdbcPassword}")
	public String JDBC_PASSWORD;
	
	@Value("${lmi.dbSchema}")
	public String DB_SCHEMA;
	
	@Autowired
	public Environment env;

	protected void preProcess() throws Exception{
		if (StringUtils.isEmpty(BASE_URL))
			throw new Exception("lmi.baseurl property is not set");

		if (StringUtils.isEmpty(USER_ID))
			throw new Exception("lmi.userid property is not set");

		if (StringUtils.isEmpty(PASSWORD))
			throw new Exception("lmi.password property is not set");

		if (StringUtils.isEmpty(CHANNEL_ID))
			throw new Exception("lmi.channelid property is not set");

		if (StringUtils.isEmpty(OUTPUT_FOLDER))
			throw new Exception(
					"lmi.reportoutputfolder property is not set");

		if (StringUtils.isEmpty(JDBC_URL))
			throw new Exception("lmi.jdbcURL property is not set");

		if (StringUtils.isEmpty(JDBC_USERID))
			throw new Exception("lmi.jdbcUserId property is not set");

		if (StringUtils.isEmpty(JDBC_PASSWORD))
			throw new Exception("lmi.jdbcPassword property is not set");
	}
	protected String getAuthCode(String email, String password) throws Exception {
		String authCode = "";
		try {
			String authURL = BASE_URL
					+ "requestAuthCode.aspx?email={email}&pwd={pwd}";
			Map<String, String> params = new HashMap<String, String>();
			params.put("email", email);
			params.put("pwd", password);
			LOG.info("GetAuthCode URL: " + authURL);
			RestTemplate restTemplate = restTemplateFactory.newRestTemplate();
			String response = restTemplate.getForObject(authURL, String.class,
					params);
			LOG.info("GetAuthCode Response: " + response);

			if (!response.startsWith("OK"))
				throw new Exception("Invalid Response");

			authCode = response.substring(response.indexOf("AUTHCODE:") + 9);
			LOG.info("Authorization Code:" + authCode);
		} catch (Exception e) {
			LOG.error("Error in getting authorizationcode:" + e);
			throw new Exception(e);
		}
		return authCode;
	}

	protected void setReportDate(String beginDate, String endDate, String authCode)
			throws Exception {
		try {
			String setReportDateURL = BASE_URL
					+ "setReportDate.aspx?bdate={bdate}&edate={edate}&authcode={authcode}";
			Map<String, String> params = new HashMap<String, String>();
			params.put("bdate", beginDate);
			params.put("edate", endDate);
			params.put("authcode", authCode);

			LOG.info("SetReportDate URL: " + setReportDateURL);
			RestTemplate restTemplate = restTemplateFactory.newRestTemplate();
			String response = restTemplate.postForObject(setReportDateURL, "",
					String.class, params);
			LOG.info("SetReportDate Response: " + response);

			if (!response.startsWith("OK"))
				throw new Exception("Invalid Response");
		} catch (Exception e) {
			LOG.error("Error in setting report date:" + e);
			throw new Exception(e);
		}
	}

	protected void setReportTime(String beginTime, String endTime, String authCode)
			throws Exception {
		try {
			String setReportTimeURL = BASE_URL
					+ "setReportTime.aspx?btime={btime}&etime={etime}&authcode={authcode}";
			Map<String, String> params = new HashMap<String, String>();
			params.put("btime", beginTime);
			params.put("etime", endTime);
			params.put("authcode", authCode);

			LOG.info("SetReportTime URL: " + setReportTimeURL);
			RestTemplate restTemplate = restTemplateFactory.newRestTemplate();
			String response = restTemplate.postForObject(setReportTimeURL, "",
					String.class, params);
			LOG.info("SetReportTime Response: " + response);

			if (!response.startsWith("OK"))
				throw new Exception("Invalid Response");
		} catch (Exception e) {
			LOG.error("Error in setting report time:" + e);
			throw new Exception(e);
		}
	}

	protected void setReportTimeZone(String timeZone, String authCode)
			throws Exception {
		try {
			String setReportTimeZoneURL = BASE_URL
					+ "setTimeZone.aspx?timezone=-{timezone}&authcode={authcode}";
			Map<String, String> params = new HashMap<String, String>();
			params.put("timezone", timeZone);
			params.put("authcode", authCode);

			LOG.info("SetReportTimeZoneURL: " + setReportTimeZoneURL);
			RestTemplate restTemplate = restTemplateFactory.newRestTemplate();
			String response = restTemplate.postForObject(setReportTimeZoneURL,
					"", String.class, params);
			LOG.info("SetReportTimeZoneURL Response: " + response);

			if (!response.startsWith("OK"))
				throw new Exception("Invalid Response");
		} catch (Exception e) {
			LOG.error("Error in setting report time zone:" + e);
			throw new Exception(e);
		}
	}

	protected void setReportArea(String reportId, String authCode)
			throws Exception {
		try {
			String setReportAreaURL = BASE_URL
					+ "setReportArea.aspx?area={area}&authcode={authcode}";
			Map<String, String> params = new HashMap<String, String>();
			params.put("area", reportId);
			params.put("authcode", authCode);

			LOG.info("SetReportAreaURL: " + setReportAreaURL);
			RestTemplate restTemplate = restTemplateFactory.newRestTemplate();
			String response = restTemplate.postForObject(setReportAreaURL, "",
					String.class, params);
			LOG.info("SetReportArea Response: " + response);

			if (!response.startsWith("OK"))
				throw new Exception("Invalid Response");
		} catch (Exception e) {
			LOG.error("Error in setting report area:" + e);
			throw new Exception(e);
		}
	}

	protected void setReportOutputType(String outputType, String authCode)
			throws Exception {
		try {
			String setReportOutputTypeURL = BASE_URL
					+ "setOutput.aspx?output={output}&authcode={authcode}";
			Map<String, String> params = new HashMap<String, String>();
			params.put("output", outputType);
			params.put("authcode", authCode);

			LOG.info("SetReportOutTypeURL: " + setReportOutputTypeURL);
			RestTemplate restTemplate = restTemplateFactory.newRestTemplate();
			String response = restTemplate.postForObject(
					setReportOutputTypeURL, "", String.class, params);
			LOG.info("SetReportOutType Response: " + response);

			if (!response.startsWith("OK"))
				throw new Exception("Invalid Response");
		} catch (Exception e) {
			LOG.error("Error in setting report output type:" + e);
			throw new Exception(e);
		}
	}

    protected String getReport(String channelId, String authCode) throws Exception {
        return getReport(null, channelId, authCode);
    }

	protected String getReport(String reportUrl, String channelId, String authCode)
			throws Exception {
		String report = "";
		try {
			String getReportURL = BASE_URL
					+ (StringUtils.isEmpty(reportUrl)?"getReport.aspx?":reportUrl)+"node={node}&authcode={authcode}";
			Map<String, String> params = new HashMap<String, String>();
			params.put("node", channelId);
			params.put("authcode", authCode);
			LOG.info("GetReport URL: " + getReportURL);
			RestTemplate restTemplate = restTemplateFactory.newRestTemplate();
			String response = restTemplate.getForObject(getReportURL,
					String.class, params);
			LOG.debug("GetReport Response: " + response);

			if (!response.startsWith("OK"))
				throw new Exception("Invalid Response");

			report = response.substring(response.indexOf("OK") + 2);
			LOG.debug("Report:" + report);
		} catch (Exception e) {
			LOG.error("Error in getting report:" + e);
			throw new Exception(e);
		}
		return report;
	}

    protected Date getLastRunDate( String jdbcURL,
                                 String jdbcUserId, String jdbcPassword, String dbSchema, String dbSql)
            throws Exception {
        Connection dbCon = null;
        Statement stmt = null;
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        Date lastRunDate = null;

        try {
            dbCon = getDBConnection(jdbcURL, jdbcUserId, jdbcPassword);
            stmt = dbCon.createStatement();
            rs = stmt.executeQuery(dbSql);
            if (rs != null && rs.next()) {
                lastRunDate = rs.getDate(1);
            }
            LOG.info("Last run date for this job is :"+lastRunDate);
            if (lastRunDate == null) {
                lastRunDate = DateUtils.addDays(new Date(), -7);
            }
        } catch (Exception ignore) {

        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ignore) {
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ignore) {
                }
            }
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ignore) {
                }
            }
            if (dbCon != null) {
                try {
                    dbCon.close();
                } catch (Exception ignore) {
                }
            }
        }
        return lastRunDate;
    }

    protected abstract List<IData> parseReportData(String reportDataStr) throws Exception ;

	protected abstract void insertIntoDB(List<IData> dataList, String jdbcURL,
			String jdbcUserId, String jdbcPassword, String dbSchema) throws Exception ;


    protected String cleanXMLString(String dirtyXMLString){
        String cleanXMLString = null;
        Pattern pattern = null;
        Matcher matcher = null;
        pattern = Pattern.compile("[\\000]*");
        matcher = pattern.matcher(dirtyXMLString);
        if (matcher.find()) {
            cleanXMLString = matcher.replaceAll("");
        }
        return cleanXMLString;
    }

    private static Connection getDBConnection(String jdbcURL,
			String jdbcUserId, String jdbcPassword) throws Exception {

		Connection dbConnection = null;

		try {

			Class.forName("oracle.jdbc.driver.OracleDriver");

			dbConnection = DriverManager.getConnection(jdbcURL, jdbcUserId,
					jdbcPassword);
			return dbConnection;

		} catch (Exception e) {

			LOG.error("Error in creating DB connection");
			throw e;

		}

	}

}