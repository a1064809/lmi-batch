package com.bestbuy.geeksquad.remoteservices;

import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;

public class RestErrorHandler extends DefaultResponseErrorHandler {

	private static Logger logger = LoggerFactory.getLogger(RestErrorHandler.class);

	@Override
	public void handleError(ClientHttpResponse response) throws IOException {
		logger.error("======Rest Client Error Report ========");
		logger.error("Status Message : " + response.getStatusText());
		logger.error("Status Code : " + response.getStatusCode().toString());
		logger.error("Status Code Reason: " + response.getStatusCode().getReasonPhrase());

		logger.error("Error Body : " + IOUtils.toString(response.getBody()));

		if (logger.isDebugEnabled()){
			for(String str: response.getHeaders().keySet()){
				logger.debug("Header Key : " + str + ", Value : " + response.getHeaders().get(str));
			}
		}

		logger.error("======Rest Client Error Report ========");
		super.handleError(response);
	}

}
