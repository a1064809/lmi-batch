package com.bestbuy.geeksquad.remoteservices;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.StringUtils;

/*
 * Author: Mahesh Sunkara
 * This is a scheduled job. Job interval is configured in applicationContext.xml.
 * Whenever this Job runs it makes list of API calls in below sequence get Agent's LMI Hierarchy.
 * Parse the report output and dump it in DB
 */
public class LMIAPIAgentHierarchyJob extends Base {

	private static final Logger LOG = LoggerFactory
			.getLogger(LMIAPIAgentHierarchyJob.class);

	public void process() {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		LOG.info("Start of LMI Feedback Job:"
				+ dateFormat.format(System.currentTimeMillis()));

		try {

			preProcess();
			String authCode = getAuthCode(USER_ID, PASSWORD);

			Date endDate = DateUtils.addDays(new Date(), -1);

			SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");

			Date begDate = DateUtils.addDays(new Date(), -7);
			setReportDate(formatter.format(begDate), formatter.format(endDate),
					authCode);

			setReportTime("00:00:00", "23:59:59", authCode);

			Calendar c = Calendar.getInstance();
			c.setTime(new Date()); // your date; omit this line for current date
			int offset = c.get(Calendar.DST_OFFSET);

			// offset 0 means no DST, any other value (most likely 3600000)
			// means that this date is affected by DST
			// Haven't tested in DST time - hope it works
			setReportTimeZone((offset == 0 ? "360" : "300"), authCode);

            setReportOutputType(OUTPUT_TYPE_XML, authCode);

            String reportText = getReport("gethierarchy.aspx?isnode=false&", CHANNEL_ID, authCode);

			String currentDateTime = (new SimpleDateFormat("ddMMyyyy_HH-mm-ss"))
					.format(System.currentTimeMillis());
			FileUtils.writeStringToFile(new File(OUTPUT_FOLDER + "LMIAGENTHIERARCHY"
					+ currentDateTime + "." + OUTPUT_TYPE_TXT), reportText);
			LOG.info("Report is written to file:" + OUTPUT_FOLDER  + "LMIAGENTHIERARCHY"
					+ currentDateTime + "." + OUTPUT_TYPE_TXT);

			List<IData> dataList = parseReportData(reportText);

			if (dataList == null || dataList.size() == 0)
				LOG.info("No rows found in the report output file");
			else
				insertIntoDB(dataList, JDBC_URL, JDBC_USERID, JDBC_PASSWORD,
						DB_SCHEMA);

		} catch (Exception e) {
			LOG.error("Unexpected error occured in the job: " + e);
		}

		LOG.info("End of LMI Feedback Job:" + dateFormat.format(System.currentTimeMillis()));
	}

    protected List<IData> parseReportData(String reportXMLStr) throws Exception {

		StringBuffer tempStr = new StringBuffer().append("<root>").append(reportXMLStr).append("</root>");
		reportXMLStr = tempStr.toString();
		InputStream inputStream = new ByteArrayInputStream(
				reportXMLStr.getBytes());
        BufferedReader reader = null;
        List<IData> dataList = new ArrayList<IData>();
        LMIAgentHierarchyData lmiAgentHierarchyData = new LMIAgentHierarchyData();
        try {
            reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while (reader.ready()){
                line = reader.readLine().trim();
                if(StringUtils.isEmpty(line)){
                    continue;
                }
                // Split line at ':'
                String[] toks = line.split(":");
                if("NodeId".equalsIgnoreCase(toks[0])){
                    lmiAgentHierarchyData = new LMIAgentHierarchyData();
                    if(toks.length > 1) lmiAgentHierarchyData.setNodeId(toks[1]);
                } else if("ParentID".equalsIgnoreCase(toks[0]) && toks.length > 1){
                    lmiAgentHierarchyData.setParentId(toks[1]);
                } else if("Name".equalsIgnoreCase(toks[0]) && toks.length > 1){
                    lmiAgentHierarchyData.setName(toks[1]);
                } else if("Email".equalsIgnoreCase(toks[0]) && toks.length > 1){
                    lmiAgentHierarchyData.setEmail(toks[1]);
                } else if("Description".equalsIgnoreCase(toks[0]) && toks.length > 1){
                    lmiAgentHierarchyData.setDescription(toks[1]);
                } else if("Status".equalsIgnoreCase(toks[0]) && toks.length > 1){
                    lmiAgentHierarchyData.setStatus(toks[1]);
                } else if("Type".equalsIgnoreCase(toks[0])){
                    if(toks.length > 1) {
                        lmiAgentHierarchyData.setType(toks[1]);
                    }
                    // adding to list
                    dataList.add(lmiAgentHierarchyData);
                }
            }
        } finally {
            if (reader != null) reader.close(); // dont throw an NPE because the file wasn't found.
        }
		//LOG.debug("dataList:" + dataList);
		return dataList;
	}

	protected void insertIntoDB(List<IData> dataList, String jdbcURL,
			String jdbcUserId, String jdbcPassword, String dbSchema)
			throws Exception {
		Connection dbCon = null;
		Statement stmt = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;

		String selectQuery = "select count(*) from " + dbSchema
				+ ".AGENT_LMI_HIERARCHY where JOB_RUN_DATE = trunc(sysdate)";
		String insertQuery = "INSERT INTO "
				+ dbSchema
				+ ".AGENT_LMI_HIERARCHY(JOB_RUN_DATE, LMI_ID, A_ID, NAME, EMAIL_ADDRESS, PID, TYPE, CREATE_DATE) VALUES(trunc(sysdate), ?, ?, ?, ?, ?, ?,sysdate)";
		try {
			dbCon = getDBConnection(jdbcURL, jdbcUserId, jdbcPassword);
			dbCon.setAutoCommit(false);
			stmt = dbCon.createStatement();
			rs = stmt.executeQuery(selectQuery);
			int noOfRows = 0;
			if (rs != null && rs.next()) {
				noOfRows = rs.getInt(1);
			}

			if (noOfRows > 0) {
				LOG.info("Stopped further processing. Job has already run for yesterday's LOGIN_DATE.");
			} else {
				LOG.info("Inserting records into database:" + dataList.size());
				int recCount = 0;
				pstmt = dbCon.prepareStatement(insertQuery);
				for (IData obj : dataList) {
					LMIAgentHierarchyData data = (LMIAgentHierarchyData) obj;
					if (++recCount % 100 == 0) {
						pstmt.executeBatch();
						pstmt.clearBatch();
					}

					if (data.getNodeId() != null)
						pstmt.setString(1, data.getNodeId());
					else
						pstmt.setNull(1, Types.VARCHAR);

					if (data.getaId() != null)
						pstmt.setString(2, data.getaId());
					else
						pstmt.setNull(2, Types.VARCHAR);

					if (data.getName() != null)
						pstmt.setString(3, data.getName());
					else
						pstmt.setNull(3, Types.TIMESTAMP);

					if (data.getEmail() != null)
						pstmt.setString(4, data.getEmail());
					else
						pstmt.setNull(4, Types.VARCHAR);

					if (data.getParentId() != null)
						pstmt.setString(5, data.getParentId());
					else
						pstmt.setNull(5, Types.INTEGER);

					if (data.getType() != null)
						pstmt.setString(6, data.getType());
					else
						pstmt.setNull(6, Types.VARCHAR);

					pstmt.addBatch();
				}
				if (recCount != 0)
					pstmt.executeBatch();
				dbCon.commit();
			}
		} catch (Exception e) {
			if (dbCon != null)
				dbCon.rollback();
			throw e;
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception ignore) {
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (Exception ignore) {
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (Exception ignore) {
				}
			}
			if (dbCon != null) {
				try {
					dbCon.close();
				} catch (Exception ignore) {
				}
			}
		}

	}

	private static Connection getDBConnection(String jdbcURL,
			String jdbcUserId, String jdbcPassword) throws Exception {

		Connection dbConnection = null;

		try {

			Class.forName("oracle.jdbc.driver.OracleDriver");

			dbConnection = DriverManager.getConnection(jdbcURL, jdbcUserId,
					jdbcPassword);
			return dbConnection;

		} catch (Exception e) {

			LOG.error("Error in creating DB connection");
			throw e;

		}

	}

	private class LMIAgentHierarchyData implements IData {

		private String nodeId;

        private String aId;

        private String parentId;

        private String name;

        private String email;

        private String description;

        private String status;

        private String type;

        private String getNodeId() {
            return nodeId;
        }

        private void setNodeId(String nodeId) {
            this.nodeId = nodeId;
        }

        private String getaId() {
            return aId;
        }

        private void setaId(String aId) {
            this.aId = aId;
        }

        private String getParentId() {
            return parentId;
        }

        private void setParentId(String parentId) {
            this.parentId = parentId;
        }

        private String getName() {
            return name;
        }

        private void setName(String name) {
            this.name = name;
        }

        private String getEmail() {
            return email;
        }

        private void setEmail(String email) {
            this.email = email;
        }

        private String getDescription() {
            return description;
        }

        private void setDescription(String description) {
            this.description = description;
        }

        private String getStatus() {
            return status;
        }

        private void setStatus(String status) {
            this.status = status;
        }

        private String getType() {
            return type;
        }

        private void setType(String type) {
            this.type = type;
        }
    }

    public static void main(String[] args) {
		try {
			String reportString = FileUtils.readFileToString(new File(
					"C:\\LMIAgentHierarchyReport12052015_23-01-44.XML"));
			// System.out.println(reportString);
			LMIAPIAgentHierarchyJob job = new LMIAPIAgentHierarchyJob();
			List<IData> dataList = job.parseReportData(reportString);
			job.insertIntoDB(dataList, "jdbc:oracle:thin:@localhost:1521:xe",
					"andyg_dev", "andyg_dev", "ANDYG_DEV");
			System.out.println(dataList);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


}